const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    text: { type: String, required: true },
    completed: { type: Boolean, default: false},
    userId: { type: String, required: true}
}, { timestamps: true});

module.exports = mongoose.model('Note', NoteSchema);