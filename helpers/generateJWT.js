const jwt = require('jsonwebtoken');

module.exports = function (username, id) {
    return jwt.sign(
        {username, id},
        process.env.SECRET_KEY,
        {expiresIn: '24h'}
    )
}