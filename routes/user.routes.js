const Router = require('express');
const router = new Router();
const userController = require('../controllers/user.controller');
const authMiddleware = require('../middleware/authMiddleware');

router.get('/me', userController.getUser);
router.patch('/me', authMiddleware, userController.editUser);
router.delete('/me', userController.deleteUser);

module.exports = router;
