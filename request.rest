###
POST http://127.0.0.1:8080/api/auth/register
Content-Type: application/json

{
  "username": "Pierce",
  "password": "123456"
}

###
POST http://127.0.0.1:8080/api/auth/login
Content-Type: application/json

{
  "username": "Pierce",
  "password": "123456"
}

###
PATCH  http://localhost:8080/api/notes/62f68dd65b8f617f09cbd035
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkVub2NoIiwiaWQiOiI2MmY2OGRkNjViOGY2MTdmMDljYmQwMmMiLCJpYXQiOjE2NjAzMjUzMzQsImV4cCI6MTY2MDQxMTczNH0.1IdDTUcBlalMQDYnUaXder_pb0KhqROdO47Xv0dqEmE

###
GET http://localhost:8080/api/notes/62f68dd65b8f617f09cbd035
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IkVub2NoIiwiaWQiOiI2MmY2OGRkNjViOGY2MTdmMDljYmQwMmMiLCJpYXQiOjE2NjAzMjUzMzQsImV4cCI6MTY2MDQxMTczNH0.1IdDTUcBlalMQDYnUaXder_pb0KhqROdO47Xv0dqEmE

###
POST http://localhost:8080/api/notes/
Content-Type: application/json
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBpZXJjZSIsImlkIjoiNjJmNjlhZTEyNTJmMjY5OGJiY2RiMTliIiwiaWF0IjoxNjYwMzI5NjIwLCJleHAiOjE2NjA0MTYwMjB9.yXJKO2lLusGOfy6LkxO441am9KIYo_FK6EOh_Cb4_NY

{
  "text": "hello word note 1"
}

###
PUT http://localhost:8080/api/notes/62f69e9ec264fe300db3bb20
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBpZXJjZSIsImlkIjoiNjJmNjlhZTEyNTJmMjY5OGJiY2RiMTliIiwiaWF0IjoxNjYwMzI5NjIwLCJleHAiOjE2NjA0MTYwMjB9.yXJKO2lLusGOfy6LkxO441am9KIYo_FK6EOh_Cb4_NY
Content-Type: application/json

{
  "text": "Hello new notes"
}

###

GET http://localhost:8080/api/notes/62f69e9ec264fe300db3bb20
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IlBpZXJjZSIsImlkIjoiNjJmNjlhZTEyNTJmMjY5OGJiY2RiMTliIiwiaWF0IjoxNjYwMzI5NjIwLCJleHAiOjE2NjA0MTYwMjB9.yXJKO2lLusGOfy6LkxO441am9KIYo_FK6EOh_Cb4_NY

